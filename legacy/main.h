#ifndef _MAIN_H
 #define _MAIN_H

#include <curses.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>		
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "types.h"
#include "uart.h"

//extern void usleep(int);

#endif //_MAIN_H
