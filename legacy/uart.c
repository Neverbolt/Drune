#include "main.h"
#include "uart.h"

int uart0_filestream = -1;
FILE *readstream = -1;
int usb_stream = -1;
int data_length;		//length of transmitting data string

//>> Adding checksum and transmitting  data
//------------------------------------------------------------------------------------------------------
void transmit_data(serial_data_struct* pdata_package) {
	if (uart0_filestream != -1) {
		int count = write(uart0_filestream, pdata_package->txrxdata,
				pdata_package->cmdLength);
		if (count < 0) {
			printf("UART TX error\n");
		}
	}
}

//>> Adding address and command ID, encoding and checksum
//------------------------------------------------------------------------------------------------------
void create_serial_frame(u8 address, u8 cmdID, u16 cmdLength,
		serial_data_struct* pdata_package) {
	unsigned char ptr = 0;
	unsigned int pt = 0;
	u8 a, b, c;

	pdata_package->txrxdata[0] = '#';               	// Start-Byte
	pdata_package->txrxdata[1] = 'a' + address;        	// Adress
	pdata_package->txrxdata[2] = cmdID;               	// Command

	while (cmdLength) {
		if (cmdLength) {
			a = pdata_package->data[ptr++];
			cmdLength--;
		} else
			a = 0;
		if (cmdLength) {
			b = pdata_package->data[ptr++];
			cmdLength--;
		} else
			b = 0;
		if (cmdLength) {
			c = pdata_package->data[ptr++];
			cmdLength--;
		} else
			c = 0;
		pdata_package->txrxdata[pt++] = '=' + (a >> 2);
		pdata_package->txrxdata[pt++] = '='
				+ (((a & 0x03) << 4) | ((b & 0xf0) >> 4));
		pdata_package->txrxdata[pt++] = '='
				+ (((b & 0x0f) << 2) | ((c & 0xc0) >> 6));
		pdata_package->txrxdata[pt++] = '=' + (c & 0x3f);
	}

	unsigned int tmpCRC = 0;

	int i = 0;
	for (; i < pt; i++) {
		tmpCRC += pdata_package->txrxdata[i];
	}

	tmpCRC %= 4096;

	pdata_package->txrxdata[pt++] = '=' + tmpCRC / 64;
	pdata_package->txrxdata[pt++] = '=' + tmpCRC % 64;
	pdata_package->txrxdata[pt++] = '\r';
}

void Decode64(u16 cmdLength, unsigned char *ptrOut, data_rx_st* pdata_package) {
	unsigned char ptr = 0;
	unsigned int pt = 0;
	u8 a, b, c, d;

	while (cmdLength) {
		a = pdata_package->data[pt++] - '=';
		b = pdata_package->data[pt++] - '=';
		c = pdata_package->data[pt++] - '=';
		d = pdata_package->data[pt++] - '=';

		if (pt > pdata_package->count - 2)
			break;

		if (cmdLength--)
			ptrOut[ptr++] = (a << 2) | (b >> 4);
		else
			break;
		if (cmdLength--)
			ptrOut[ptr++] = ((b & 0x0f) << 4) | (c >> 2);
		else
			break;
		if (cmdLength--)
			ptrOut[ptr++] = ((c & 0x03) << 6) | d;
		else
			break;
	}
}

//>> Initializing serial interface
//------------------------------------------------------------------------------------------------------
void uart_init() {
	uart0_filestream = open(SERIAL, O_RDWR | O_NOCTTY | O_NDELAY);
	readstream = fopen(SERIAL, "r");
	if (uart0_filestream == -1) {
		printf(
				"Error - Unable to open UART.  Ensure it is not in use by another application\n");
	}
	//UART Options
	struct termios options;
	tcgetattr(uart0_filestream, &options);
	options.c_cflag = B57600 | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart0_filestream, TCIFLUSH);
	tcsetattr(uart0_filestream, TCSANOW, &options);
	//UART Options
}

#define BUFFER_LENGTH 1024

//>> Reading a message from the serial stream
//------------------------------------------------------------------------------------------------------
u8 read_data(u8 *buffer) {
	u16 firstEmptySpace = 0;
	u16 availableSpace = BUFFER_LENGTH;
	u16 startOfMessage = 0;
	u16 endOfMmessage = 0;
	u16 bytesInBuffer = 0;
	u8 sentinel = '\r';
	u8 cmd_start = '#';

	u16 numberRead = 1;
	if (getc(readstream) != cmd_start) {
		return 0;
	}

	// Fill up the buffer.
	availableSpace = BUFFER_LENGTH - firstEmptySpace;
	u16 i = 0;
	u8 current_char;
	for (; i < BUFFER_LENGTH; i++) {
		current_char = getc(readstream);
		if (current_char == sentinel) {
			break;
		}
		buffer[i] = current_char;
	}
	return i;
}
